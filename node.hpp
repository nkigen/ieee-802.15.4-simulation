#include "project.hpp"
#include<iostream>
#include<vector>
#include<string>
#include<deque>
#include<iterator>

#define LOCAL_PROCESSING_TIME 10
class NodePosition{
  COORD_TYPE _x;
  COORD_TYPE _y;
  float _radius;

public:
  static int _num_nodes;
  
  NodePosition(COORD_TYPE x,COORD_TYPE y,float r,bool tmp=true);
  COORD_TYPE getX() const;
  COORD_TYPE getY() const;
  float getRadius() const;
  
  bool isNeighbour(const NodePosition &np);
  bool operator==(const NodePosition &np);
  //NodePosition& operator=(const NodePosition &np);
  
  static int getNumNodes();
  

  //   ~NodePosition();
};

class Message;
class WirelessInterface;

class Node :public COMPONENT{
  
  std::unique_ptr<RANDVAR> _interval;
  std::vector<std::shared_ptr<Node> > _neighbours;
  std::shared_ptr<NodePosition> _pos;
  std::shared_ptr< WirelessInterface> _interface;
  int _cnt_sent;
  int _cnt_recv;
  int _cnt_fwd;

  std::deque<std::shared_ptr<Message> > _queue;
  std::shared_ptr<Node> _me_ptr;
  int _msg_trans_time; /*Transmission time per unit of message*/
  
  class Exc : public MetaSim::BaseExc {
  public:
    Exc(std::string msg) : 
    MetaSim::BaseExc(msg, "Link", "link.cpp") {}
  };
public:
      static int _total_send;
      static int _total_recv;
  MetaSim::GEvent <Node> _evt_send;
  MetaSim::GEvent <Node> _evt_recv;
  
  
  Node(const char *name,int _tt);
  Node(const char * name, std::shared_ptr<NodePosition> pos,int _tt);
  
  // ~Node();
  void setSharedPtr(std::shared_ptr<Node> n);
  
  void setWirelessInterface(std::shared_ptr<WirelessInterface> w);
  std::shared_ptr<WirelessInterface> getWirelessInterface();
  
  void setInterval(std::unique_ptr<RANDVAR> i); 
  
  void setPosition(std::shared_ptr<NodePosition> pos);
  std::shared_ptr<NodePosition> getPosition();
  
  void newNeighbour(std::shared_ptr<Node> n);
  void getNeighbours(std::vector<std::shared_ptr<Node> > &i);
  
  void endRun();
  void newRun();
  
  void onMessageSent(std::shared_ptr<Node> n);
  void onMessageReceive();
  
  void onSend(EVENT *e);
  void onReceive(EVENT *e);
  
  void receiveMessage(std::shared_ptr<Message> m);
  
  static void addSent(int n);//{_total_send+=n;}
  static int getSent();//{return _total_send;}
  
  static void addRecv(int n);
  static int getRecv();
  
private:
  /*checks if the node should foward the message or "consume" it*/
  bool fowardMessage(std::shared_ptr<Message> m);
  /*gets the position of the next hop node*/
  int getNextHop(NodePosition &cur);
  bool isSourceNode(const NodePosition &np) const;
  
};