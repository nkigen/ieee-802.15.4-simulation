#include "message.hpp"
//#include "interface.hpp"
#include "node.hpp"

using namespace std;
using namespace MetaSim;

Message::Message(std::shared_ptr<Node> s,std::shared_ptr<Node> d,int l): _src(s),_dest(d),_len(l),_is_corrupt(false)
{
  
}

std::shared_ptr<Node> Message::getDestNode(){
  
 return _dest;
 
}

shared_ptr<Node> Message::getSourceNode(){
    return _src;
  
}

void Message::setStartTime(TIMER t)
{
  _start_time=t;
}

TIMER Message::getStartTime(){  
  return _start_time;
}

void Message::setTransmitTime(TIMER t){
  
  _trans_time=t*_len;
}

TIMER Message::getTransmitTime(){
  return _trans_time;
}

void Message::setArrivalTime(TIMER t){
  _arr_time=t;
}

TIMER Message::getArrivalTime(){
  return _arr_time;
}

void Message::setSourceNode(std::shared_ptr<Node> n){
  _src=n;
}
void Message::setDestNode(std::shared_ptr<Node> n){

  _dest=n;
}


  