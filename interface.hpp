#ifndef __INTERFACE__HPP
#define __INTERFACE__HPP
#include "project.hpp"
#include<deque>
// #include "node.hpp"
// #include "message.hpp"
// #include "interface.hpp"
// #include "link.hpp"
class Node;
class Message;
class Link;
class NodePosition;

class WirelessInterface: public COMPONENT{
  
  std::shared_ptr<Node> _node;
  int _cnt_recv;
  int _cnt_sent;
  std::deque<std::shared_ptr<Message> > _queue;
  //std::deque<Message> _queue_in;
  bool _is_transmit;
  std::shared_ptr<Link> _channel;
  std::unique_ptr<RANDVAR> _interval;
  class Exc : public MetaSim::BaseExc {
  public:
    Exc(std::string msg) : 
    MetaSim::BaseExc(msg, "Link", "link.cpp") {}
  };
public:
  MetaSim::GEvent <WirelessInterface> _evt_recv;
  MetaSim::GEvent <WirelessInterface> _evt_foward; 
  
  WirelessInterface(const char *name,std::shared_ptr<Link> l);
  
  void sendMessage(std::shared_ptr<Message> m);
  void receiveMessage(std::shared_ptr<Message> m);
  
  void setNode(std::shared_ptr<Node> n);
  std::shared_ptr<Node> getNode();
  
  void onSendMessage(std::shared_ptr<Message> m);
  void onReceiveMessage(std::shared_ptr<Message> m);
  
  bool isTransmit();
  
  void onSend(EVENT *e);
  void onReceive(EVENT *e);
  
  void endRun();
  void newRun();
  
private:
  void getNextAttempt();
};

#endif