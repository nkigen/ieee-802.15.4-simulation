#ifndef __SIMULATE_HPP__
#define _SIMULATE_HPP__

#include<vector>
#include "project.hpp"
#include "interface.hpp"
#include "link.hpp"
#include "message.hpp"
#include "node.hpp"


class SimStat: public MetaSim::StatCount{

public:
  SimStat(const char *name);
  void probe(MetaSim::Event *e);
  void attach(COMPONENT *e);
};

class SimulateNetwork{

  std::vector<std::shared_ptr<Node> > _nodes; 
 
  //vector<std::shared_ptr<WirelessInterface> > _interfaces;
  std::shared_ptr<Link> _channel;
  int _radius;
  int _msg_len;
  int _trans_time;
   int _sim_len;
  int _num_runs;
  int _ret_len;
public:
  SimulateNetwork(int rad,int mlen,float ttime,int slen,int nruns);
  void initSimulation(int nodes);
  void runSimulation();
  
private:
  void computeNodeNeighbours();
  void setNodeIntervals();
  bool isNeighbour(NodePosition a,NodePosition b);
  
};

#endif