#include "link.hpp"
#include<iostream>
#include<algorithm>
#include "node.hpp"
#include "message.hpp"
#include "interface.hpp"
using namespace std;
using namespace MetaSim;


Link::Link(const char *name):
COMPONENT(name),_cnt_processed(0),_cnt_collision(0),_evt_start_transmit(),_evt_end_transmit(){
  
    _interval=move(unique_ptr<RANDVAR>(new UniformVar(0,LINK_INTERVAL)));
  register_handler(_evt_start_transmit,this,&Link::onStartTransmit);
  register_handler(_evt_end_transmit,this,&Link::onEndTransmit);
}

void Link::setInterface(std::shared_ptr<WirelessInterface> wi){
  _interfaces.push_back(wi);
}

bool Link::senseTransmitNeighbour(WirelessInterface &wi) {
  //cout<<"link::senseTransmitNeighbour...."<<wi.getName()<<endl;
  shared_ptr<Node> _n=wi.getNode();
  vector<shared_ptr<Node> > _nbr; 
  _n->getNeighbours(_nbr);
  //  cout<<"link::senseTransmitNeighbour....nbr size"<<_nbr.size()<<endl;
  if(_active_nodes.size()==0)
    return false;
  if(_nbr.size()>0)
  {
    vector<shared_ptr<Node> >::iterator i=_active_nodes.begin();
    /*TODO:check if intersection of _nbr and _active_nodes is zero then return false else return true*/
    if(isIntersect(_active_nodes,_nbr))
      return true;
    /*TODO: correct*/
  }
  else
  {
    throw Exc(getName()+" No neighbours present!!\n");
  }
  //cout<<getName()<<" TRANSMIT ERROR NEIGHBOUR IN ACTION!!"<<endl;
  return false;
}

void Link::onStartTransmit(EVENT *e){
  // cout<<getName()<<" Link::onStartTransmit "<<SIMUL.getTime()<<endl;
  if(_messages.size())
  {
    shared_ptr<Message> _m=_messages.front();
    _messages.pop_front();
    _m->setStartTime((TIMER)SIMUL.getTime());
    _active_nodes.push_back(_m->getSourceNode());
    ++_cnt_processed;
    _on_transit.push_back(_m);
    if(!_evt_end_transmit.isInQueue())
      _evt_end_transmit.post(SIMUL.getTime()+(TIMER)_m->getTransmitTime()+(TIMER)_interval->get());
    //  cout<<getName()<<" Link::onStartTransmit message sent"<<endl;
  }
  else
  {
    throw Exc(getName()+"Event Raised on Empty Message Queue!!\n");
    /*TODO: check whats wrong if this is ever true*/
    //  cout<<getName()<<" ERROR: Link:onStartTransmit"<<endl;
  }
  
  if(_messages.size())
    _evt_start_transmit.process();
  
}

bool Link::isColliding(std::shared_ptr<Message> m){
  /*TODO: complete the interferance model ASAP*/
  bool _bad=false;
  std::vector<std::shared_ptr<Node> > _nbr;
  auto _src=m->getSourceNode();
  _src->getNeighbours(_nbr);
  vector<shared_ptr<Node> >::iterator _itr_active=_active_nodes.begin();
  vector<shared_ptr<Node> >::iterator _itr_nbr=_nbr.begin();
  deque<shared_ptr<Message> >::iterator _itr_ot=_on_transit.begin();
  
  while(_itr_ot!=_on_transit.end()){
    auto _tsrc=(*_itr_ot)->getSourceNode();
    if(std::find(_nbr.begin(),_nbr.end(),_tsrc)!=_nbr.end() && *_itr_ot!=m)
    {
      if((*_itr_ot)->getStartTime()==m->getStartTime()){
	m->setCorrupt();
	(*_itr_ot)->setCorrupt();
	//cout<<getName()<<" current time "<<SIMUL.getTime()<<" time send "<<(*_itr_ot)->getStartTime()<<endl;
	_bad=true;
      }
    }
    _itr_ot++;
  }
  return _bad;
}

void Link::onEndTransmit(EVENT *e){
  //  cout<<getName()<<" Link::onEndTransmit "<<SIMUL.getTime()<<endl;
  if(_on_transit.size())
  {
    shared_ptr<Message> _m=_on_transit.front();
    _on_transit.pop_front();
    _m->setArrivalTime((TIMER)SIMUL.getTime());
    /*TODO: check for collisions here*/
    
    /*TODO: check collision end*/
    if(isColliding(_m)){
    //  cout<<getName()<<" Collision Detected..Message will be dropped"<<endl;
    } 
    
    std::vector<shared_ptr<Node> >::iterator _i=_active_nodes.begin();
    while(_i!=_active_nodes.end())
    {
      if(*_i==_m->getSourceNode())
      {
	_i=_active_nodes.erase(_i);
	if(!_m->isCorrupt())
	_m->getDestNode()->getWirelessInterface()->receiveMessage(_m);
	break;
      }
      _i++;
    } 
  }
  else
  {
    throw Exc(getName()+"Event Raised on Empty Message In Transmission Queue!!\n");
    
    //  cout<<"ERROR: Link:onEndTransmit"<<endl;
    
  }
  if(_on_transit.size() && !_evt_end_transmit.isInQueue())
    _evt_end_transmit.post(SIMUL.getTime()+(TIMER)_interval->get());
}

void Link::receiveMessage(std::shared_ptr<Message> m){
 
  // cout<<getName()<<" Link::receiveMessage. Message received from "<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<" "<<SIMUL.getTime()<<endl;
  _messages.push_back(m);
  if(!_evt_start_transmit.isInQueue())
    _evt_start_transmit.post(SIMUL.getTime()+(TIMER)_interval->get());
}

void Link::endRun(){
  std::cout<<"****************************************************************"<<std::endl;
  std::cout<<"Total messages sent "<<Node::getSent()<<". Total recieved "<<Node::getRecv()<<std::endl; 
 // cout<<getName()<<" Link endRun _cnt_processed "<<_cnt_processed<<endl;
}
void Link::newRun(){
  // cout<<getName()<<" Link::newRun "<<SIMUL.getTime()<<endl;
  
  //_cnt_collision=0;
  //_cnt_processed=0;
  _active_nodes.clear();
  _on_transit.clear();
  _messages.clear();
  //  cout<<"Link newRun"<<endl;
}

/*private members*/

bool Link::isIntersect(std::vector<std::shared_ptr<Node> > &n1,std::vector<std::shared_ptr<Node> > &n2)
{
  // cout<<"Link::isIntersect..."<<endl;
  
  int _c=0;
  std::vector<shared_ptr<Node> >::iterator i1=n1.begin();
  std::vector<shared_ptr<Node> >::iterator i2=n2.begin();
  
  while(i1!=n1.end())
  {
    i2=n2.begin();
    while(i2!=n2.end())
    {
      if(*i2==*i1)
	return true;
      i2++;
    }
    
    ++i1;
  }
  return false;
}
void Link::transmitSuccess(std::shared_ptr<Message> m){
}
void Link::transmitFailure(std::shared_ptr<Message> m){
}