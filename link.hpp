#ifndef __LINK__HPP
#define __LINK__HPP
#include "project.hpp"
#include<vector>
#include<iterator>
#include<deque>
// #include "node.hpp"
// #include "message.hpp"
// #include "interface.hpp"
class Message;
class Node;
class NodePosition;
class WirelessInterface;


class Link: public COMPONENT{
  
  int _cnt_processed;
  int _cnt_collision;
  std::deque<std::shared_ptr<Message> > _messages;
  std::deque<std::shared_ptr<Message> > _on_transit;
  std::unique_ptr<RANDVAR> _interval;
  std::vector<std::shared_ptr<WirelessInterface> > _interfaces;
  std::vector<std::shared_ptr<Node> > _active_nodes;
  
  class Exc : public MetaSim::BaseExc {
  public:
    Exc(std::string msg) : 
    MetaSim::BaseExc(msg, "Link", "link.cpp") {}
  };
  
public:
  MetaSim::GEvent <Link> _evt_start_transmit;
  MetaSim::GEvent <Link> _evt_end_transmit;
  
  Link(const char *name);
  
  void setInterface(std::shared_ptr<WirelessInterface> wi);
  
  bool senseTransmitNeighbour(WirelessInterface &wi);
  
  void onStartTransmit(EVENT *e);
  void onEndTransmit(EVENT *e);
  
  void receiveMessage(std::shared_ptr<Message> m);
  
  void endRun();
  void newRun();
  
  
private: 
  bool isIntersect(std::vector<std::shared_ptr<Node> > &n1,std::vector<std::shared_ptr<Node> > &n2);
  bool isColliding(std::shared_ptr<Message> m);
  void transmitSuccess(std::shared_ptr<Message> m);
  void transmitFailure(std::shared_ptr<Message> m);
};
#endif