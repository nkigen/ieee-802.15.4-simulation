#include "interface.hpp"
#include "node.hpp"
#include "message.hpp"
#include "link.hpp"
#include<iostream>


using namespace std;
using namespace MetaSim;
WirelessInterface::WirelessInterface(const char *name,std::shared_ptr<Link> l):
_cnt_sent(0),_cnt_recv(0),_is_transmit(false),COMPONENT(name),_channel(l),_evt_recv(),_evt_foward()
{
  _interval=move(unique_ptr<RANDVAR>(new UniformVar(0,INTERFACE_INTERVAL)));
  register_handler(_evt_recv, this, &WirelessInterface::onReceive);
  register_handler(_evt_foward, this, &WirelessInterface::onSend);
  
}

void WirelessInterface::sendMessage(std::shared_ptr<Message> m){
  //  cout<<getName()<<" WirelessInterface::sendMessage...message from"<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<endl;
 
    shared_ptr<WirelessInterface> dest_interface=m->getDestNode()->getWirelessInterface();
    _queue.push_back(m);
    if(!_evt_foward.isInQueue())
    _evt_foward.post(SIMUL.getTime()+(TIMER)_interval->get());
    // cout<<"WirelessInterface::sendMessage...finished. queue size"<<_queue.size()<<endl;

}

void WirelessInterface::receiveMessage(std::shared_ptr<Message> m){
 //  cout<<getName()<<" WirelessInterface::receiveMessage...message from"<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<endl;

  shared_ptr<Node> _n=m->getDestNode();
  _n->receiveMessage(m);
  if(!_evt_recv.isInQueue())
  _evt_recv.post(SIMUL.getTime()+(TIMER)_interval->get());
  onReceiveMessage(m);
}

void WirelessInterface::setNode(std::shared_ptr<Node> n){
  _node=n;
}

std::shared_ptr<Node> WirelessInterface::getNode(){
  /*TODO return this pointer as a shared_ptr*/
  return _node;
  //return (std::shared_ptr<Node>(_node));  /*TODO: Verify this....not sure*/
}

void WirelessInterface::onSendMessage(std::shared_ptr<Message> m){
  
//  cout<<getName()<<" Message from "<<m->getSourceNode()->getName()<<" fowarded to node "<<m->getDestNode()->getName()<<endl;
  
}
void WirelessInterface::onReceiveMessage(std::shared_ptr<Message> m){
  ++_cnt_recv;
 // cout<<getName()<<" Message Received from "<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<endl;
  
}

void WirelessInterface::onSend(EVENT *e){
   //   cout<<getName()<<" WirelessInterface::onSend..."<<endl;
  if(!_queue.empty())
  {
       // cout<<"WirelessInterface::onSend...queue size ok"<<endl;
    //shared_ptr<WirelessInterface> _me=make_shared<WirelessInterface>(this,getName(),);
    if(_channel->senseTransmitNeighbour(*this))
    {
      /*TODO: Add support for retries*/
      if(!_evt_foward.isInQueue())
      _evt_foward.post(SIMUL.getTime()+(TIMER)_interval->get());
      return;
    }
    ++_cnt_sent;
      onSendMessage(_queue.front());
    _channel->receiveMessage(_queue.front());
    _queue.pop_front();
    if(!_queue.empty())
        if(!_evt_foward.isInQueue())
	  _evt_foward.post(SIMUL.getTime()+(TIMER)_interval->get());
  }
  else
  {
    /*TODO: error WirelessInterface::onSend()*/
    throw Exc(getName()+"Event Raised on Empty Queue!!\n");
  //  cout<<getName()<<"ERROR WirelessInterface::onSend() _queue empty!!"<<endl;
  }
  
}
void WirelessInterface::onReceive(EVENT *e){
  ++_cnt_recv;
  //_node->_evt_recv.post(SIMUL.getTime()); //inform node of new message
  //_node->receiveMessage();
 // cout<<getName()<<" WirelessInterface::receiveMessage and Message received"<<endl;
}

void WirelessInterface::endRun(){
  //cout<<getName()<<" Messages sent:"<<_cnt_sent<<". Messages recvd:"<<_cnt_recv<<endl;
}
void WirelessInterface::newRun(){
  _cnt_recv=0;
  _cnt_sent=0;
  _queue.clear();
  
}


/*TODO: Complete this function*/
void WirelessInterface::getNextAttempt(){
  static int num_attempts=MAX_RETRY_ATTEMPTS;
  
}


