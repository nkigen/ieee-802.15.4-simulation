#ifndef __PROJECT_HPP__
#define __PROJECT_HPP__
#include<metasim.hpp>
#include<memory>

#define MSG_LEN_MAX 128
#define MSG_LEN_MIN 64
#define MAX_RETRY_ATTEMPTS 5

#define LINK_INTERVAL 30
#define INTERFACE_INTERVAL 40

/**
 * typedefs
 */
#define COMPONENT MetaSim::Entity
#define TIMER MetaSim::Tick
typedef int COORD_TYPE; //units for the coordinate of a node
#define RANDVAR MetaSim::RandomVar
#define EVENT MetaSim::Event
#define COUNTER MetaSim::StatCount

#endif