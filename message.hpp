#ifndef __MESSAGE_HPP
#define __MESSAGE_HPP
#include "project.hpp"
//#include "node.hpp"



class Node;
//class WirelessInterface;
class Message
{
  std::shared_ptr<Node> _src;
  std::shared_ptr<Node> _dest;
  int _len;
  TIMER _start_time;
  TIMER _trans_time;
  TIMER _arr_time;
  bool _is_corrupt;
public:
  Message(std::shared_ptr<Node> s,std::shared_ptr<Node> d,int l);
  
  void setDestNode(std::shared_ptr<Node> n);
  std::shared_ptr<Node> getDestNode();
  
  void setSourceNode(std::shared_ptr<Node> n);
  std::shared_ptr<Node> getSourceNode();
  
  void setStartTime(TIMER t);
  TIMER getStartTime();
  
  void setTransmitTime(TIMER t);
  TIMER getTransmitTime();
  
  void setArrivalTime(TIMER t);
  TIMER getArrivalTime();
  
  inline void setCorrupt(){_is_corrupt=true;}
  inline bool isCorrupt(){return _is_corrupt;}
  
 
};


#endif