#include "simulate.hpp"
#include<iostream> 
#include<cmath>
#include<string>
#define NSIZE sqrt(_nodes.size()/2)

using namespace std;
using namespace MetaSim;
SimulateNetwork::SimulateNetwork(int rad,int mlen,float ttime,int slen,int nruns): _radius(rad),_msg_len(mlen),
_trans_time(ttime),_sim_len(slen),_num_runs(nruns),_ret_len(100)
{
  _channel=make_shared<Link>("IEEE802.15.4 Channel");
}

void SimulateNetwork::initSimulation(int nodes)
{
  cout<<"init simulation..."<<endl;
  int i,j;
  nodes=pow(nodes,2);
  
  for(i=0;i<sqrt(nodes);i++){
    for(j=0;j<sqrt(nodes);j++){
      auto pos=shared_ptr<NodePosition>(new NodePosition(i,j,1.00,false));
      string name("Node ");
      string iface("Interface ");
      name+=to_string(i)+","+to_string(j);
      iface+=to_string(i)+","+to_string(j);
      //cout<<"("<<i<<","<<j<<") node name "<<name<<" interface name : "<<iface<<endl;
      auto n=make_shared<Node>(name.c_str(),pos,_trans_time);
      n->setSharedPtr(n);
      auto interface=make_shared<WirelessInterface>(iface.c_str(),_channel);
      interface->setNode(n);
      n->setWirelessInterface(interface);
      _nodes.push_back(n);
      _channel->setInterface(interface);
    }
  }
  /*TODO: set wireless interfaces to link*/
  computeNodeNeighbours();
}

void SimulateNetwork::runSimulation(){
  cout<<"run simulation..."<<endl;
  setNodeIntervals();
  
  /*TODO: run simulation*/
  for(int i=0;i<5;i++)
    SIMUL.run(_sim_len,_num_runs);
  
}
void SimulateNetwork::setNodeIntervals(){
  //int _size=_nodes.size();
  // int pos=0;
  // while(pos<_size){
    vector<shared_ptr<Node> >::iterator _i=_nodes.begin();
    while(_i!=_nodes.end())
    {
      (*_i)->setInterval(std::unique_ptr<RANDVAR>(new UniformVar(1,_ret_len)));
      ++_i;
    }
    // ++pos;
    //}
    
}
void SimulateNetwork::computeNodeNeighbours(){
  cout<<"computing neighbours..."<<endl;
  for(int i=0;i<(int)_nodes.size();i++)
  { 
    for(int j=0;j<(int)_nodes.size();j++)
    {
      if(i!=j)
	if(isNeighbour(*(_nodes[i]->getPosition()),*(_nodes[j]->getPosition())))
	  _nodes[i]->newNeighbour(_nodes[j]);
    }
  }
  
}

bool SimulateNetwork::isNeighbour(NodePosition a,NodePosition b){
  return a.isNeighbour(b); 
  
}

/**************************************SimStat Definition*************************************/

SimStat::SimStat(const char *name):StatCount(name){
}
void SimStat::probe(MetaSim::Event *e){
  record(1);
}
void SimStat::attach(COMPONENT *e){
  
  Link *_channel = dynamic_cast<Link *>(e);
  if (_channel == NULL) 
    throw BaseExc("Please, specify a Link!");
  
 // l->_corrupt_evt.addStat(this);
}








