#include "simulate.hpp"
#include<iostream>



int main(int argc, char **argv)
{
  SimulateNetwork netsim(1,64,1,40000,4);
  netsim.initSimulation(4); //pass the numberof nodes here
  netsim.runSimulation();

  return 0;
}