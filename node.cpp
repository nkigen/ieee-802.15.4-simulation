
#include "node.hpp"
#include<iostream>
#include<algorithm> 
#include "message.hpp"
#include "interface.hpp"


int NodePosition::_num_nodes;
int Node::_total_recv;
int Node::_total_send;


using namespace std;
using namespace MetaSim;

Node::Node(const char *name,int _tt):
COMPONENT(name),_evt_send(),_evt_recv(),_interval(nullptr),_cnt_sent(0),_cnt_recv(0),_cnt_fwd(0),_msg_trans_time(_tt){
  _total_send=0;
  _total_recv=0;
  register_handler(_evt_recv, this, &Node::onReceive);
  register_handler(_evt_send, this, &Node::onSend);
  
}
Node::Node(const char * name, std::shared_ptr<NodePosition> pos,int _tt):COMPONENT(name),_evt_send(),_evt_recv(),_interval(nullptr),_cnt_sent(0),_cnt_recv(0),_cnt_fwd(0),_msg_trans_time(_tt){
  _pos=pos;
  _total_send=0;
  _total_recv=0;
  register_handler(_evt_recv, this, &Node::onReceive);
  register_handler(_evt_send, this, &Node::onSend);
  
}

void Node::setSharedPtr(std::shared_ptr<Node> n){
  _me_ptr=n;
}
void Node::setWirelessInterface(shared_ptr<WirelessInterface> w)
{
  _interface=w;
}

shared_ptr<WirelessInterface> Node::getWirelessInterface()
{
  
  return _interface;
  
}

void Node::setPosition(shared_ptr<NodePosition> pos)
{
  _pos=pos;
}
std::shared_ptr<NodePosition> Node::getPosition()
{
  return _pos;
}

void Node::endRun()
{
  Node::addSent(_cnt_sent);
  Node::addRecv(_cnt_recv);
 
//  cout<<getName()<<".SEND "<< _cnt_sent<<". RECEIVED "<<_cnt_recv<<" FORWARDED "<<_cnt_fwd<<endl;
  
}

bool Node::isSourceNode(const NodePosition &np) const{
  if(np.getX()==0)
    return true;
  if(np.getY()==0)
    return true;
  return false;
}

void Node::newRun()
{
  //cout<<"Node:.newRun"<<endl;
  _cnt_recv=0;
  _cnt_sent=0;
  _cnt_fwd=0;
  /*******************DEGUB**********************************************/
  if(_queue.size())
    _queue.clear();
  
  
  /*******************DEGUB**********************************************/
  if(_neighbours.size()==0){
    throw Exc(getName()+"FATAL ERROR: Node has NO Neighbours!!\n");
    
  }
  NodePosition _np=*getPosition();
  MetaSim::UniformVar len(MSG_LEN_MIN,MSG_LEN_MAX);
  int ret=0;
  if(!isSourceNode(_np)){
    // cout<<getName()<<" not a source node...exiting..."<<endl;
    return;
  }
  /*TODO: Add messages from neighbours to _queue ONLY to source nodes*/
  
  if(_np.getX()==(int)0)
  {
    ++_cnt_sent;
    /*if NodePosition= (0,0) send a message to NodePosition(0,1)*/
    if(_np.getY()==(int)0)
    {
      ++_cnt_sent;
      NodePosition _tmp(_np.getX(),_np.getY()+1,_np.getRadius());
      int ret=0;
      if((ret=getNextHop(_tmp))==-1)
	throw Exc(getName()+"Next Hop for the Message Not found!!\n");
      
      //    cout<<getName()<<" Message to ("<<_tmp.getX()<<", "<<_tmp.getY()<<")"<<endl;
	
	auto m=make_shared<Message>(_me_ptr,_neighbours[ret],(int)len.get());
	m->setTransmitTime(_msg_trans_time);
	_queue.push_back(m);
    }
    // cout<<getName()<<" im in queue"<<endl;
    NodePosition _tmp(_np.getX()+1,_np.getY(),_np.getRadius());
    
    if((ret=getNextHop(_tmp))==-1)
      throw Exc(getName()+"Next Hop for the Message Not found!!\n");
    
    //   cout<<getName()<<" Message to ("<<_tmp.getX()<<", "<<_tmp.getY()<<")"<<endl;
      
      auto m=make_shared<Message>(_me_ptr,_neighbours[ret],(int)len.get());
      m->setTransmitTime(_msg_trans_time);
      _queue.push_back(m);
  }
  if(_np.getY()==(int)0 && _np.getX()!=(int)0)
  {
    ++_cnt_sent;
    // cout<<getName()<<" im in queue"<<endl;
    NodePosition _tmp(_np.getX(),_np.getY()+1,_np.getRadius());
    
    if((ret=getNextHop(_tmp))==-1)
      return;
    /*TODO:correct this dummy*/
    //  cout<<getName()<<" Message to ("<<_tmp.getX()<<", "<<_tmp.getY()<<")"<<endl;
    auto m=make_shared<Message>(_me_ptr,_neighbours[ret],(int)len.get());
    m->setTransmitTime(_msg_trans_time);
    _queue.push_back(m);    
  }
  if(!_evt_send.isInQueue())
    _evt_send.post(SIMUL.getTime()+(TIMER)_interval->get());
  //_evt_send.process();
    // cout<<getName()<<" New RUN"<<endl;
}

void Node::onMessageSent(shared_ptr<Node> n)
{
  // cout<<getName()<<" Node::onMessageSent Message sent from node "<< getName()<<" to node "<<n->getName() <<endl;
}
void Node::onMessageReceive(){
  ++_cnt_recv;
  //cout<<getName()<<" Message Sink reached!!!!!"<<endl;
}

void Node::onSend(EVENT *e){
  // cout<<getName()<<" Node:.Onsend >>>_queue->size="<<_queue.size()<<endl;
  if(_queue.empty())
    return;
  std::shared_ptr<Message> m=_queue.front();
  _queue.pop_front();
  onMessageSent(m->getDestNode());
  _interface->sendMessage(m);
  if(!_queue.empty()){
    //  cout<<getName()<<" New message scheduled to be sent later. current time "<<SIMUL.getTime()<<endl;
    _evt_send.post(SIMUL.getTime()+(TIMER)_interval->get());   
  }
  
}

void Node::onReceive(EVENT *e){
  onMessageReceive();
}

void Node::newNeighbour(std::shared_ptr<Node> n)
{
  _neighbours.push_back(n); 
}


void Node::getNeighbours(std::vector<std::shared_ptr<Node> > &i)
{
  std::copy(_neighbours.begin(),_neighbours.end(),std::back_inserter(i));
  //  cout<<"Node::getNeighbours....size "<<_neighbours.size()<<endl;
}

void Node::receiveMessage(shared_ptr<Message> m)
{
  // cout<<getName()<<" Node::receiveMessage "<<"...message from"<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<endl;
  // cout<<getName()<<" Node::receiveMessage.....checking if fowarding is necessary..."<<endl;
  if(fowardMessage(m))
  {
    // cout<<getName()<<" Node::receiveMessage...fowarding necessary...message will be fowarded "<<endl;
    ++_cnt_fwd;
    /*TODO: add onfoward event*/
    _queue.push_back(m);
    if(!_evt_send.isInQueue())
      _evt_send.post(SIMUL.getTime()+(TIMER)_interval->get()); 
    //   cout<<getName()<<" Intermediete node: Message Fowarded to next node"<<endl;
      return;
  }
  if(!_evt_recv.isInQueue())
    _evt_recv.post(SIMUL.getTime()); //same as .process!
    // _evt_recv.process();
  //cout<<"Message received at node"<<endl;
}

/*Private Methods*/
/**m-messages
 * 
 */
bool Node::fowardMessage(std::shared_ptr<Message> m)
{
  /*TODO: Verify this dummy logic*/
  // cout<<getName()<<" Node::fowardMessage...message from"<<m->getSourceNode()->getName()<<" to "<<m->getDestNode()->getName()<<endl;
  
  std::shared_ptr<Node> _n=m->getDestNode();
  std::shared_ptr<Node> _src=m->getSourceNode(); 
  
  NodePosition _dpos=*(_n->getPosition());
  NodePosition _spos=*(_src->getPosition());
  int _xl=-1+sqrt(NodePosition::getNumNodes()); 
  // cout<<"DUMMY CHECK*********************************_xl>>> "<<_xl<<endl;
  if((_dpos.getX()==_xl && _spos.getX()!=_xl) || (_dpos.getY()==_xl && _spos.getY()!=_xl))
    return false;
  
  COORD_TYPE _x=0,_y=0;
  if(_dpos.getX()==_spos.getX()){
    _x=_dpos.getX();
    _y=_dpos.getY()+1; 
  }
  else if(_dpos.getY()==_spos.getY()){
    _x=_dpos.getX()+1;
    _y=_dpos.getY();
  }
  NodePosition _newdest(_x,_y,_dpos.getRadius());
  
  m->setSourceNode(_n);
  std::vector<std::shared_ptr<Node> > _nbr;
  _n->getNeighbours(_nbr);
  std::vector<shared_ptr<Node> >::iterator _i=_nbr.begin();
  while(_i!=_nbr.end()){
    // cout<<"Node::fowardMessage looop..."<<endl;
    if(_newdest==*((*_i)->getPosition()))
      break;
    _i++;
  }
  if(_i==_nbr.end()){
    throw Exc(getName()+"Message Fowarding Error!! \n");
    
  }
  // cout<<getName()<<" message destination"<<(*_i)->getName()<<endl;
  m->setDestNode(*_i);
  
  //     cout<<"node::fowardMessage TERMINATES FINE"<<endl;
  /*TODO: verify this logic*/
  return true;
}

void Node::setInterval(std::unique_ptr<RANDVAR> i){
  
  _interval=move(i);
}



int Node::getNextHop(NodePosition &cur)
{
  //cout<<"Node::getNextHop size "<<_neighbours.size()<<endl;
  int i=0;
  while(i<_neighbours.size())
  {
    // cout<<_neighbours[i]->getPosition()->getX()<<","<<_neighbours[i]->getPosition()->getY()<<"::"<<cur.getX()<<","<<cur.getY()<<endl;
    if(*(_neighbours[i]->getPosition())==cur)
      return i;
    ++i;
  }
  return -1;
}

void Node::addSent(int n){
  _total_send+=n;
  
}
int Node::getSent(){
  return _total_send;
  
}

void Node::addRecv(int n){
  _total_recv+=n;
  
}
int Node::getRecv(){
  return _total_recv;
  
}


/*Class NodePosition Implementantion*/
NodePosition::NodePosition(COORD_TYPE x,COORD_TYPE y,float r,bool tmp):_x(x),_y(y),_radius(r)
{
  if(!tmp)
    ++NodePosition::_num_nodes;
}
COORD_TYPE NodePosition::getX() const{
  return _x;
}
COORD_TYPE NodePosition::getY() const{
  return _y;
}
float NodePosition::getRadius() const{
  return _radius;
}

bool NodePosition::operator==(const NodePosition &np){
  
  if(getX()==np.getX() && getY()==np.getY())
    return true;
  return false;
}

/*
 * NodePosition& operator=(const NodePosition &np)
 * {
 *  _x=np.getX();
 *  _y=np.getY();
 *  _radius=np.getRadius();
 *  return *this;
 *  
 * }
 */

int NodePosition::getNumNodes(){
  return _num_nodes;
}

bool NodePosition::isNeighbour(const NodePosition &np)
{
  if(_x==np.getX())
  {
    if(abs(_y-np.getY())==1)
      return true; 
  }
  
  if(_y==np.getY())
  {
    if(abs(_x-np.getX())==1)
      return true;    
  }
  return false;
}

// NodePosition::~NodePosition(){
  //   --NodePosition::_num_nodes;
  // }